#
# Copyright (C) 2019 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

LIBARTPALETTE_1 { # introduced=31
  global:
    # --- VERSION 01 API ---
    PaletteSchedSetPriority; # systemapi
    PaletteSchedGetPriority; # systemapi
    PaletteWriteCrashThreadStacks; # systemapi
    PaletteTraceEnabled; # systemapi
    PaletteTraceBegin; # systemapi
    PaletteTraceEnd; # systemapi
    PaletteTraceIntegerValue; # systemapi
    PaletteAshmemCreateRegion; # systemapi
    PaletteAshmemSetProtRegion; # systemapi
    PaletteCreateOdrefreshStagingDirectory; # systemapi
    PaletteShouldReportDex2oatCompilation; # systemapi
    PaletteNotifyStartDex2oatCompilation; # systemapi
    PaletteNotifyEndDex2oatCompilation; # systemapi
    PaletteNotifyDexFileLoaded; # systemapi
    PaletteNotifyOatFileLoaded; # systemapi
    PaletteShouldReportJniInvocations; # systemapi
    PaletteNotifyBeginJniInvocation; # systemapi
    PaletteNotifyEndJniInvocation; # systemapi

  local:
    *;
};

LIBARTPALETTE_2 { # introduced=33
  global:
    # --- VERSION 02 API ---
    PaletteReportLockContention; # systemapi
} LIBARTPALETTE_1;

LIBARTPALETTE_3 { # introduced=34
  global:
    # --- VERSION 03 API ---
    PaletteSetTaskProfiles; # systemapi
} LIBARTPALETTE_2;

LIBARTPALETTE_4 { # introduced=36
  global:
    # --- VERSION 04 API ---
    PaletteDebugStoreGetString; # systemapi
} LIBARTPALETTE_3;
